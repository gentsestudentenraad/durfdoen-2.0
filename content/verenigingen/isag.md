---
titel: ISAG
id: isag
naam: ISAG
verkorte_naam: ISAG
konvent: ik
contact: ugent.isag@gmail.com
website: https://isagugent.wordpress.com/
social:
  - platform: facebook
    link: https://www.facebook.com/groups/isag.ugent/
themas:
  -  internationaal
---

International Students Association of Ghent

ISAG was founded in 1960 by the international students of University of Ghent with the aim to increase the social and cultural understanding between the international students and Belgian community. This is achieved through the activities that ISAG organizes during the year. All the board are international students. Most activities are scheduled for Saturdays.
