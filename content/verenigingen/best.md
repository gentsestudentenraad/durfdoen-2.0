---
titel: Board of European Students of Technology Ghent
id: best
naam: Board of European Students of Technology Ghent
verkorte_naam: Board of European Students of Technology Ghent
contact: ghent-board@best.eu.org
website: https://vtk.ugent.be/best/
social:
  - platform: facebook
  - platform: instagram
  - platform: linkedin
    link: https://www.linkedin.com/company/board-of-european-students-of-technology-ghent/about/?
konvent: ik
themas:
  -  internationaal
  - wetenschap
---

BEST is an international student organisation uniting technology students from all over Europe. Our goal is to develop activities for those students in order to help them become more internationally minded.
In our activities we always seek the balance between fun and learning, as we believe this is the best way to improve ourselves. We do all of this through the help of local BEST groups, who host the activities organised by BEST and get the opportunity to organise events of their own. BEST Ghent is one of those local groups, organising a bunch of international and local events, such as our engineering competition EBEC, our summer course and a lot of internal events.
